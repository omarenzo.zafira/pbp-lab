from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid):
        form.save()
        redirect('/lab-4')
    response = {'form': form}
    return render(request, 'lab4_form.html', response)
    
def note_list(request):
    note = Note.objects.all() 
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)