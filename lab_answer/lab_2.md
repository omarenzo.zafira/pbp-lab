1. Apakah perbedaan antara JSON dan XML?
Terdapat beberapa perbedaaan antara JSON dengan XML. Berikut saya paparkan perbedaan diantara keduanya:

1) JSON merupakan format yang ditulis dengan bahasa pemrograman JavaScript yang dapat dibaca dengan mudah secara logis oleh manusia. 
Sedangkan, XML bukanlah merupakan bahasa pemrograman, melainkan bahasa markup dengan menggunakan tag dalam mendefinisikan elemen-elemennya sehingga membuat strukturnya menjadi lebih besar dan tidak semudah JSON untuk dibaca.

2) JSON menggunakan memori yang sangat sedikit dalam pengaplikasiannya sehingga cocok untuk grafik objek yang besar. 
Sedangkan, XML membutuhkan ruang memori yang besar.

3) Dikarenakan ukuran dokumen yang sangat kecil, membuat JSON lebih cepat untuk diuraikan oleh mesin JavaScript dan karena hal tersebutlah transfer data semakin lebih cepat.
Sedangkan, XML membutuhkan waktu yang lama dalam penguraiannya yang menyebabkan transfer data menjadi lebih lambat.

4) XML dapat menambahkan komentar serta metadata di dalamnya. Selain itu, XML juga mendukung namespaces.
Sedangkan, JSON kebalikan dari XML, sehingga tidak dapat menambahkan komentar, metadata, dan tidak mendukung namespaces di dalamnya.

5) XML dapat berisikan banyak tipe data yang kompleks seperti table, charts, dan tipe data non-primitif lainnya.
Sedangkan, JSON hanya dapat berisikan tipe data String, array Boolean, angka, dan objek yang berisikan tipe data primitif lainnya.


2. Apakah perbedaan antara HTML dan XML?
Terdapat beberapa perbedaaan antara HTML dengan XML. Berikut saya paparkan perbedaan diantara keduanya:

1) Seperti yang telah kita ketahui HTML berfokus pada penyajian data.
Sedangkan, XML berfokus pada transfer serta penyimpanan data.

2) XML bersifat Case Sensitive, dengan artian peka dengan perbedaan dari huruf besar dengan huruf kecil ("Enzo" dengan "enzo" berbeda)
Sedangkan, HTML bersifat Case Insensitive, yaitu tidak peka dengan perbedaan dari huruf besar dengan huruf kecil ("Enzo" dengan "enzo" sama)

3) Pada XML mendukung namespaces di dalamnya sedangkan HTML tidak mendukung namespaces.

4) Pada XML tag penutup wajib ada pada setiap tag yang digunakan.
Sedangkan, HTML tidak wajib untuk ada tag penutup.

5) Pada XML tag dapat dikembangkan serta tidak ditentukan sebelumnya.
Sedangkan, HTML memiliki tag yang tidak dapat dikembangkan atau terbatas serta telah ditentukan sebelumnya.

Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://perbedaan.budisma.net/perbedaan-html-dan-xml.html
https://blogs.masterweb.com/perbedaan-xml-dan-html/#:~:text=XML%20adalah%20singkatan%20dari%20eXtensible,HTML%20difokuskan%20pada%20penyajian%20data.
