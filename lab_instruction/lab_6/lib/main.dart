import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
    runApp(MaterialApp(
        theme: ThemeData(
            primaryColor: new Color(0xFFC37B89),
        ),
    debugShowCheckedModeBanner: false,
    home: LoginPage(),
    ));
}

class LoginPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: new Color(0xFFC37B89),
                title: Text('HelPINK U',
                    style: TextStyle(
                        color: Colors.white,
                    ), // style
                ),
            ),
            body: SingleChildScrollView(
                child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                    ),// box
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                            SizedBox(height: 130,),
                            Container(
                                height: 400,
                                width: 350,
                                decoration: BoxDecoration(
                                    color: new Color(0xFFC37B89),
                                    borderRadius: BorderRadius.circular(15)
                                ), // deco
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                        SizedBox(height:30,),
                                        Text('Masuk',
                                            style: TextStyle(
                                                fontSize: 35,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                            ), // style
                                        ),// Text
                                        SizedBox(height:50,),
                                        Container(
                                            width: 300,
                                            child: TextField(
                                                decoration: InputDecoration(
                                                    labelText: 'Username',
                                                    hintText: "Username",
                                                    suffixIcon: Icon(FontAwesomeIcons.user, size: 20),
                                                    border: OutlineInputBorder(
                                                        borderRadius: new BorderRadius.circular(5)),
                                                ), // input
                                            ), //field
                                        ),// container
                                        SizedBox(height:10,),
                                        Container(
                                            width: 300,
                                            child: TextField(
                                                decoration: InputDecoration(
                                                    labelText: 'Password',
                                                    hintText: "Password",
                                                    suffixIcon: Icon(FontAwesomeIcons.key, size: 20),
                                                    border: OutlineInputBorder(
                                                        borderRadius: new BorderRadius.circular(5)),
                                                ), // input
                                            ), //field
                                        ),// container
                                        SizedBox(height:40,),
                                        Container(
                                            width: 250,
                                            child:RaisedButton(
                                                padding: EdgeInsets.all(12.0),
                                                onPressed: () {
                                                    print("Pressed");
                                                },
                                                color: Colors.white,
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                                                child: Text('Masuk',
                                                    style: TextStyle(
                                                        color: Colors.orange,
                                                        fontSize: 20,                                                    
                                                    ),//style
                                                ),//text
                                            ), //button
                                        ), //container
                                        SizedBox(height:50,),
                                        Text('Belum punya akun?',
                                            style: TextStyle(
                                                color: Colors.white,                                                   
                                            ),//style
                                        ),// text
                                        Text('Daftar',
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                decoration: TextDecoration.underline,
                                                color: Colors.orange,
                                            ), // style
                                        ),// Text
                                    ],// children
                                ), // column
                            ), // container
                        ], // children
                    ),//column
                ), // container
            ),// Scroll
        );// Scaffold
    }
}