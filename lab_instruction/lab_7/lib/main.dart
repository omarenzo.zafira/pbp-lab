import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Login",
    home: LoginPage(),
  ));
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _username = TextEditingController();
  TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color(0xFFC37B89),
        title: Text('HelPINK U',
          style: TextStyle(
            color: Colors.white,
          ), // style
        ),// title
      ),//appbar
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
            ),// box
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 400,
                  width: 350,
                  decoration: BoxDecoration(
                    color: new Color(0xFFC37B89),
                    borderRadius: BorderRadius.circular(15)
                  ), // deco
                  child: Column(
                    children: [
                      SizedBox(height:30,),
                      Text('Masuk',
                        style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ), // style
                      ),// Text
                      SizedBox(height:30,),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _username,
                          decoration: new InputDecoration(
                            hintText: "Username",
                            labelText: "Username",
                            suffixIcon: Icon(Icons.person),
                            border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0)),
                          ),
                          validator: (String? value) {
                            if (value!=null&&value.isEmpty) {
                              return 'Username tidak boleh kosong';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _password,
                          obscureText: true,
                          decoration: new InputDecoration(
                            hintText: "Password",
                            labelText: "Password",
                            suffixIcon: Icon(Icons.lock),
                            border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0)),
                          ),
                          validator: (String? value) {
                            if (value!=null&&value.isEmpty) {
                              return 'Password tidak boleh kosong';
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(height:10,),
                      Container(
                        width: 250,
                        child:RaisedButton(
                          child: Text(
                            "Masuk",
                            style: TextStyle(color: Colors.orange),
                          ),
                          color: Colors.white,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              var _username2 = _username.text;
                              var _password2 = _password.text;

                              print("Username: " + _username2);
                              print("Password: " + _password2);
                            }
                          },
                        ),
                      ),
                      SizedBox(height:45,),
                      Text('Belum punya akun?',
                        style: TextStyle(
                          color: Colors.white,                                                   
                        ),//style
                      ),// text
                      Text('Daftar',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                          color: Colors.orange,
                        ), // style
                      ),// Text
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}